//= require modernizr/modernizr
//= require jquery/dist/jquery
//= require_tree .
//= require foundation/js/foundation.min

$(function(){ 
  $(document).foundation(); 
  $('.need-equal-heights').equalHeights();
});